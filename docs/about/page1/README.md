---
description: 'July 21, 2021'
---

# About BudgetWatch

Know what you're budget is going to look like before you spend. Let's say that you're going grocery shopping under a strict grocery budget. You open up BudgetWatch and start a new transaction. As you place an item in your cart, you can either scan the barcode or enter the product information your self \(name and price\). This will create a pending deduction from your budget and tell you how much of your budget you'll have left, and if you're going over.

