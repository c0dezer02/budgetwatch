import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    height: 28,
  },
  text: {
    fontSize: 15,
    fontFamily: 'Inconsolata-Bold',
    height: 24,
    lineHeight: 20,
    textAlign: 'center',
  },
});

export default styles;
